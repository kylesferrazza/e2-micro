{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "e2-micro";
  buildInputs = with pkgs; [
    terraform_0_14
    ansible
  ];
}

terraform {
  backend "remote" {
    organization = "kylesferrazza"

    workspaces {
      name = "e2-micro"
    }
  }
}

variable "gcp_key" {
  type = string
}

variable "dns_name" {
  type = string
  default = "gcp"
}

variable "username" {
  type = string
  default = "kyle"
}

variable "gcp_project" {
  type = string
}

variable "email" {
  type = string
  default = "kyle.sferrazza@gmail.com"
}

variable "domain" {
  type = string
  default = "kylesferrazza.com"
}

variable "ssh_pubkey" {
  type = string
}

provider "google" {
  credentials = var.gcp_key
  project = var.gcp_project
  region  = "us-east1"
  zone    = "us-east1-b"
}

resource "google_compute_address" "public_ip" {
  name = "public-ip"
}

resource "google_compute_firewall" "default" {
  name = "web-firewall"
  network = "default"

  allow {
    protocol = "icmp"
  }

  # allow {
  #   protocol = "tcp"
  #   ports = ["80", "443"]
  # }

  source_ranges = ["0.0.0.0/0"]
  target_tags = ["e2-micro-fw"]
}

resource "google_compute_instance" "f1_vm" {
  name = "e2-micro"
  machine_type = "e2-micro"

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  tags = ["e2-micro-fw"]

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.public_ip.address
    }
  }

  metadata = {
    ssh-keys = "${var.username}:${var.ssh_pubkey}"
  }
}

output "public-ip" {
  value = google_compute_address.public_ip.address
}


variable "cloudflare_api_token" {
  type = string
}

variable "cloudflare_zone_id" {
  type = string
}

provider "cloudflare" {
  api_token = var.cloudflare_api_token
}

resource "cloudflare_record" "gcp_record" {
  zone_id = var.cloudflare_zone_id
  name = var.dns_name
  value = google_compute_address.public_ip.address
  type = "A"
  ttl = 300
}
